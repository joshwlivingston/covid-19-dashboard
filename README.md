# COVID-19 Dashboard

Welcome to my simple Shiny application for viewing COVID-19 case rates. I made this because I wanted a way to visualize the per captia moving average of new cases in areas where I had friends and family.

If I were to scale this out, I would incorporate county-level population estimates to make all counties searchable. I would have to reimagine the visualizations if I were to scale it out.
